package com.kshrd.custom_recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RecyclerViewAction {

    private RecyclerView recyclerView;
    private mRecyclerAdapter adapter;
    private List<Dataset> account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        account = new ArrayList<>();
        account.add(new Dataset("Aisaka Taiga", "I like fruites"));
        account.add(new Dataset("Umaru Chan", "I like cooking"));
        account.add(new Dataset("Lila", "I love sightseeing"));
        account.add(new Dataset("Aisaka Taiga", "I like fruites"));
        account.add(new Dataset("Umaru Chan", "I like cooking"));
        account.add(new Dataset("Lila", "I love sightseeing"));
        account.add(new Dataset("Aisaka Taiga", "I like fruites"));
        account.add(new Dataset("Umaru Chan", "I like cooking"));
        account.add(new Dataset("Lila", "I love sightseeing"));
        account.add(new Dataset("Aisaka Taiga", "I like fruites"));
        account.add(new Dataset("Umaru Chan", "I like cooking"));
        account.add(new Dataset("Lila", "I love sightseeing"));

        recyclerView = findViewById(R.id.recyclerview);
        adapter = new mRecyclerAdapter(account, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(int position) {
        Toast.makeText(this, account.get(position).getAccName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLongItemClick(int position) {
        account.remove(position);
        adapter.notifyItemRemoved(position);
    }
}