package com.kshrd.custom_recyclerview;

public class Dataset {
    private String accName;
    private String description;
    private int image;

    public String getAccName() {
        return accName;
    }

    public Dataset(String accName, String description) {
        this.accName = accName;
        this.description = description;
    }

    public Dataset(String accName, String description, int image) {
        this.accName = accName;
        this.description = description;
        this.image = image;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
