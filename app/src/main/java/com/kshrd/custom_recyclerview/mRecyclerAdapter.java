package com.kshrd.custom_recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class mRecyclerAdapter extends RecyclerView.Adapter<mRecyclerAdapter.ViewHolder>{

    private List<Dataset> account;
    private RecyclerViewAction recyclerViewAction;

    public mRecyclerAdapter(List<Dataset> account, RecyclerViewAction recyclerViewAction){
        this.account = account;
        this.recyclerViewAction = recyclerViewAction;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.my_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.accName.setText(account.get(position).getAccName().toString());
        holder.description.setText(account.get(position).getDescription().toString());
    }

    @Override
    public int getItemCount() {
        return account.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image;
        TextView accName, description;
        Button btnadd, btncancel;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            accName = itemView.findViewById(R.id.header);
            description = itemView.findViewById(R.id.description);
            btnadd = itemView.findViewById(R.id.btnadd);
            btncancel = itemView.findViewById(R.id.btncancel);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recyclerViewAction.onItemClick(getAdapterPosition());
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    recyclerViewAction.onLongItemClick(getAdapterPosition());
                    return true;
                }
            });
        }
    }
}
