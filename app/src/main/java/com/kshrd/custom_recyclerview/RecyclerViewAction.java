package com.kshrd.custom_recyclerview;

public interface RecyclerViewAction {

    void onItemClick(int position);
    void onLongItemClick(int position);

}
